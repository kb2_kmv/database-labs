
SELECT TOP 10 *
FROM dbo_student
SELECT *
FROM Reiting ORDER BY Reiting DESC
SELECT TOP 10 *
FROM Rozklad_pids
SELECT TOP 10 *
FROM Predmet_plan
SELECT TOP 10 *
FROM predmet
SELECT TOP 10 *
FROM Predmet_plan
SELECT TOP 10 *
FROM Navch_plan
SELECT TOP 10 *
FROM dbo_groups
SELECT TOP 10 *
FROM form_kontr
SELECT TOP 10 *
FROM Form_navch

-- *Сумарний рейтинг студента з кожної дисципліни.

SELECT dbo_student.[Name] AS StudentName, dbo_student.Sname AS Surname,
	predmet.Nazva AS [ClassName], SUM(Reiting.Reiting) AS ReitingSum
FROM Reiting
	INNER JOIN dbo_student ON (Reiting.Kod_student = dbo_student.Kod_stud)
	INNER JOIN Rozklad_pids ON (Reiting.K_zapis = Rozklad_pids.K_zapis)
	INNER JOIN Predmet_plan ON (Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl)
	INNER JOIN predmet ON (Predmet_plan.K_predmet = predmet.K_predmet)
		GROUP BY dbo_student.[Name], predmet.Nazva, dbo_student.Sname
			ORDER BY ReitingSum DESC

-- *Розрахувати кількість студентів у кожній групі.

SELECT dbo_groups.Kod_group AS [Group],
	SUM(CASE WHEN dbo_student.Kod_group IS NOT NULL THEN 1 ELSE 0 END) AS StudentsCount
FROM dbo_student
	RIGHT JOIN dbo_groups ON (dbo_student.Kod_group = dbo_groups.Kod_group)
		GROUP BY dbo_groups.Kod_group

-- *Розрахувати кількість дисциплін за групою.

SELECT dbo_groups.Kod_group AS [Group],
	SUM(CASE WHEN Predmet_plan.K_predmet IS NOT NULL THEN 1 ELSE 0 END) AS ClassesCount
FROM dbo_groups
	LEFT JOIN Navch_plan ON (dbo_groups.K_navch_plan = Navch_plan.K_navch_plan)
	LEFT JOIN Predmet_plan ON (Navch_plan.K_navch_plan = Predmet_plan.K_navch_plan)
		GROUP BY dbo_groups.Kod_group

-- *Розрахувати кількість проведених занять у кожній групі.

SELECT dbo_groups.Kod_group AS [Group],
	SUM(CASE WHEN Chas_all IS NOT NULL THEN Chas_all ELSE 0 END)*60/45 AS ClassesCount
FROM dbo_groups
	LEFT JOIN Navch_plan ON (dbo_groups.K_navch_plan = Navch_plan.K_navch_plan)
	LEFT JOIN Predmet_plan ON (Navch_plan.K_navch_plan = Predmet_plan.K_navch_plan)
		GROUP BY dbo_groups.Kod_group

-- *Розрахувати середній бал за групою.

SELECT dbo_groups.Kod_group AS [Group],
	AVG(CASE WHEN Reiting.Reiting IS NOT NULL THEN Reiting.Reiting ELSE 0 END) AS AverageRating
FROM Reiting
	INNER JOIN dbo_student ON (Reiting.Kod_student = dbo_student.Kod_stud)
	RIGHT JOIN dbo_groups ON (dbo_student.Kod_group = dbo_groups.Kod_group)
		GROUP BY dbo_groups.Kod_group

-- *Розрахувати середній бал з дисципліни.

SELECT predmet.Nazva AS ClassName, AVG(Reiting.Reiting) AS AverageGrade
	FROM Reiting
		INNER JOIN Rozklad_pids ON (Reiting.K_zapis = Rozklad_pids.K_zapis)
		INNER JOIN Predmet_plan ON (Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl)
		INNER JOIN predmet ON (Predmet_plan.K_predmet = predmet.K_predmet)
			GROUP BY predmet.Nazva

-- *Розрахувати поточний рейтинг студента з кожної дисципліни.

GO
CREATE OR ALTER VIEW StudntsRaiting
AS
	SELECT dbo_student.[Name] AS StudentName, dbo_student.Sname AS Surname,
		predmet.Nazva AS [ClassName], AVG(Reiting.Reiting) AS Reiting
	FROM Reiting
		INNER JOIN dbo_student ON (Reiting.Kod_student = dbo_student.Kod_stud)
		INNER JOIN Rozklad_pids ON (Reiting.K_zapis = Rozklad_pids.K_zapis)
		INNER JOIN Predmet_plan ON (Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl)
		INNER JOIN predmet ON (Predmet_plan.K_predmet = predmet.K_predmet)
	GROUP BY dbo_student.[Name], predmet.Nazva, dbo_student.Sname
GO

SELECT *
FROM StudntsRaiting
ORDER BY Reiting DESC

-- *Відобразити найменший рейтинг студентів з дисципліни.

DECLARE @LessonName varchar(50) = 'Психологія *'

SELECT TOP (1)
	*
FROM StudntsRaiting
WHERE ClassName = @LessonName
ORDER BY Reiting ASC

-- *Відобразити найбільший студентський рейтинг з дисципліни.
GO
DECLARE @LessonName varchar(50) = 'Психологія *'

SELECT TOP (1) WITH TIES
	*
FROM StudntsRaiting
WHERE ClassName = @LessonName
ORDER BY Reiting DESC

-- *Розрахувати кількість проведених занять за видами для кожної дисципліни.

SELECT Form_navch.V_form AS StudyType, SUM(Predmet_plan.Chas_all)*60/45 AS ClassesCount
FROM Predmet_plan
	INNER JOIN Navch_plan ON (Predmet_plan.K_navch_plan = Navch_plan.K_navch_plan)
	INNER JOIN Form_navch ON (Navch_plan.k_form = Form_navch.K_form)
GROUP BY Form_navch.V_form

-- *Розрахувати кількість груп за кожною спеціальністю.

SELECT Spetsialnost.Nazva AS Speciality, COUNT(*) AS GroupsCount
FROM Spetsialnost
	INNER JOIN Navch_plan ON (Spetsialnost.K_spets = Navch_plan.K_spets)
	INNER JOIN dbo_groups ON (Navch_plan.K_navch_plan = dbo_groups.K_navch_plan)
GROUP BY Spetsialnost.Nazva

-- *Запит на знищення даних з таблиці «Reiting» за визначеним кодом
-- *студента (в поле параметра вводиться прізвище студента).

DECLARE @StudentSurname varchar(50) = 'Базелюк'

DELETE FROM Reiting 
	WHERE Reiting.Kod_student IN (
		SELECT Kod_stud
			FROM dbo_student
				WHERE dbo_student.Sname = @StudentSurname
	)

-- *Запит на оновлення даних у таблиці «Reiting» – передбачити
-- *збільшення балів за модульні контролі на 15%.

-- В таблиці form_kontr Модульних контрольних немає
DECLARE @ExamType varchar(50) = 'Іспит'

UPDATE Reiting
	SET Reiting.Reiting = (
			CASE
				WHEN Reiting.Reiting + Reiting.Reiting*0.15 > 100
				THEN 100
				ELSE Reiting.Reiting + Reiting.Reiting*0.15
			END
		)
	WHERE K_zapis IN (
		SELECT K_zapis
		FROM Rozklad_pids
			INNER JOIN form_kontr ON (Rozklad_pids.Zdacha_type = form_kontr.k_fk)
		WHERE forma_kontr = @ExamType 
	)

-- *Запит на оновлення даних в таблиці «Reiting»
-- *передбачити зменшення балів за іспит на 15%.

GO
DECLARE @ExamType varchar(50) = 'Іспит'

UPDATE Reiting
	SET Reiting.Reiting = (
				CASE
					WHEN Reiting.Reiting - Reiting.Reiting*0.15 < 0
					THEN 0
					ELSE Reiting.Reiting - Reiting.Reiting*0.15
				END
		)
	WHERE K_zapis IN (
		SELECT K_zapis
		FROM Rozklad_pids
			INNER JOIN form_kontr ON (Rozklad_pids.Zdacha_type = form_kontr.k_fk)
		WHERE forma_kontr = @ExamType 
	)

-- *Запит на вставку даних до таблиці «Reiting» 
-- *передбачити вставку даних студентів визначеної
-- *групи (код пари та початковий бал задається динамічно).


GO
BEGIN TRANSACTION

	-- Create Students Codes view
	GO
	CREATE OR ALTER FUNCTION GetGroupStudentsCodes (
		@Group varchar(50)
	)
	RETURNS TABLE 
	AS
	RETURN (
		SELECT Kod_stud, ROW_NUMBER() OVER(ORDER BY Kod_stud) AS RowNumber
			FROM dbo_student 
			WHERE Kod_group = @Group
	)
	GO

	GO
	CREATE OR ALTER VIEW GroupStudentsCodes
	AS
	SELECT * FROM GetGroupStudentsCodes('ПІ-53')
	GO

	-- Declare Variables
	DECLARE @BaseGrade int = 99
	DECLARE @Present bit = 0
	DECLARE @SubjectShortCode varchar(50) = 'АКС'
	DECLARE @Group varchar(50) = 'ПІ-53'

	-- Get students count from GroupStudentsCodes view  
	DECLARE @StudentsCount SMALLINT = (
		SELECT COUNT(*) FROM GroupStudentsCodes
	)
	SELECT * FROM GroupStudentsCodes

	-- Get SubjectPlanId (Predmet plan) by studying plan for the 
	-- semestr (K_navch_plan) and SubjectShortCode
	DECLARE @SubjectPlanId int = (
		SELECT K_predm_pl 
			FROM Predmet_plan
				INNER JOIN predmet ON (Predmet_plan.K_predmet = predmet.K_predmet)
			WHERE K_navch_plan = 17 AND predmet.Nazva_skor = @SubjectShortCode
			
	)

	-- Create new schedule and get it's ID (K_zapis)
	DECLARE @ScheduleCode TABLE (ScheduleCode int)
	INSERT INTO Rozklad_pids
		OUTPUT INSERTED.K_zapis INTO @ScheduleCode
		VALUES (GETDATE(), @SubjectPlanId, @Group, 2, NULL, 1)

	-- Create interation index
	DECLARE @Index SMALLINT = 1

	-- Enter theloop of filling up students info
	WHILE @Index <= @StudentsCount
	BEGIN
		-- Get current student 
		DECLARE @CurrentStudentCode SMALLINT = (
			SELECT Kod_stud 
			FROM GroupStudentsCodes 
			WHERE RowNumber = @Index
		)

		-- Insert the student into the reting table with schedule code,
		-- base grade, and presence
		INSERT INTO Reiting
			VALUES (
					(SELECT TOP 1 ScheduleCode FROM @ScheduleCode),
					@CurrentStudentCode, @BaseGrade, @Present
				)

		SET @Index = @Index + 1
	END
COMMIT
GO

-- *Запит на вставку даних до таблиці «Para» – передбачити
-- *вставку всіх дисциплін, назва яких починається з
-- *літери «М» (дата заняття та години задаються динамічно, однакові для всіх).

-- Таблиці не існує

-- *Запит на оновлення даних – передбачити зміну порядкового номера
-- *змістовного модуля за певною дисципліною на нове значення.

-- Змістовний модуль ?????

-- *Передбачити знищення студентів з таблиці «Students» за визначеним номером групи.

DECLARE @GroupToClear varchar(50) = 'ПІ-53'

DELETE FROM dbo_student 
	WHERE Kod_group = @GroupToClear

-- *Запит на вставку даних до таблиці «Reiting» – передбачити 
-- *вставку даних студентів визначеної групи 
-- *(код пари та присутність задаються динамічно) 

GO
BEGIN TRANSACTION
	DECLARE @Grade int = 99
	DECLARE @Present bit = 1
	DECLARE @SubjectShortCode varchar(50) = 'АКС'
	DECLARE @StudentIni varchar(50) = 'Базелюк О.В.'
	
	DECLARE @Group varchar(50) = (
		SELECT Kod_group FROM dbo_student WHERE Name_ini = @StudentIni
	)
	DECLARE @StudentCode varchar(50) = (
		SELECT Kod_stud FROM dbo_student WHERE Name_ini = @StudentIni
	)
	DECLARE @SubjectPlanId int = (
		SELECT K_predm_pl 
			FROM Predmet_plan
				INNER JOIN predmet ON (Predmet_plan.K_predmet = predmet.K_predmet)
			WHERE K_navch_plan = 17 AND predmet.Nazva_skor = @SubjectShortCode
			
	)

	INSERT INTO Rozklad_pids
		VALUES (GETDATE(), @SubjectPlanId, @Group, 2, NULL, 1)

	INSERT INTO Reiting
		VALUES (34, @StudentCode, @Grade, @Present)
COMMIT
GO

-- *Передбачити оновлення даних у таблиці «Reiting» 
-- *поле «Prisutnist», для студента із визначеним
-- *прізвищем автоматично встановлюється true.

DECLARE @StudentSurname varchar(50) = 'Герасимович'

UPDATE Reiting
	SET Prisutn = 1
		WHERE Kod_student = (
			SELECT Kod_stud
				FROM dbo_student
					WHERE Sname = @StudentSurname
		)