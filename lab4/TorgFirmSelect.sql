USE torg_firm

SELECT *
FROM sotrudnik
SELECT *
FROM zakaz
SELECT *
FROM postachalnik
SELECT *
FROM tovar
SELECT *
FROM zakaz_tovar

-- вивести загальну кількість товарів на підприємстві.

SELECT COUNT(*) AS 'Загальна кількість товарів'
FROM tovar

-- вивести загальну кількість співробітників підприємства.

SELECT COUNT(*) AS 'Загальна кількість співробітників'
FROM sotrudnik

-- вивести загальну кількість постачальників підприємства.

SELECT COUNT(*) AS 'Загальна кількість постачальників'
FROM postachalnik

-- вивести кількість за кожним товаром, що придбані у поточному місяці.

SELECT Nazva, SUM(Kilkist) AS SoldItemsInTheMonth
	FROM zakaz_tovar
		INNER JOIN tovar ON (zakaz_tovar.id_tovar = tovar.id_tovar)
		INNER JOIN zakaz ON (zakaz_tovar.id_zakaz = zakaz.id_zakaz)
			WHERE DATEPART(YEAR, date_naznach) = DATEPART(YEAR, '2017-07-20')
				AND DATEPART(MONTH, date_naznach) = DATEPART(MONTH, '2017-07-20')
			GROUP BY Nazva

-- вивести суму, на яку були придбані товари у поточному місяці.

SELECT Nazva, SUM(Price * Kilkist) AS TotalSoldPrice
	FROM tovar
		INNER JOIN zakaz_tovar ON (tovar.id_tovar = zakaz_tovar.id_tovar)
			GROUP BY Nazva

-- вивести суму продажу товарів за кожним постачальником.

SELECT postachalnik.Nazva AS Supplier, SUM(Price * Kilkist) AS TotalSoldPrice
	FROM postachalnik
		INNER JOIN tovar ON (postachalnik.id_postach = tovar.id_postav)
		INNER JOIN zakaz_tovar ON (tovar.id_tovar = zakaz_tovar.id_tovar)
			GROUP BY postachalnik.Nazva

-- вивести загальну кількість замовлень за кожним постачальником, що продає молоко.

SELECT postachalnik.Nazva, COUNT(*) AS OrdersCount
	FROM postachalnik
		INNER JOIN tovar ON (postachalnik.id_postach = tovar.id_postav)
		INNER JOIN zakaz_tovar ON (tovar.id_tovar = zakaz_tovar.id_tovar)
			WHERE postachalnik.id_postach IN (
				SELECT DISTINCT postachalnik.id_postach
					FROM postachalnik
						INNER JOIN tovar ON (postachalnik.id_postach = tovar.id_postav)
							WHERE tovar.Nazva = 'Молоко'
			)
			GROUP BY postachalnik.Nazva

-- вивести середню суму, на яку замовлявся товар.

SELECT AVG(Kilkist * Price) AS AvarageProductsSoldPrice
	FROM zakaz_tovar
		INNER JOIN tovar ON (zakaz_tovar.id_tovar = tovar.id_tovar)

-- вивести вартість замовлень усіх клієнтів, що мешкають у Житомирі.

SELECT SUM(Price*Kilkist) AS TotalBoughtPrice
	FROM klient
		INNER JOIN zakaz ON (klient.id_klient = zakaz.id_klient)
		INNER JOIN zakaz_tovar ON (zakaz.id_zakaz = zakaz_tovar.id_zakaz)
		INNER JOIN tovar ON (zakaz_tovar.id_tovar = tovar.id_tovar)
			Where City = 'Житомир'

-- вивести середню ціну на товари по кожному постачальнику.

SELECT postachalnik.Nazva AS Supplier, AVG(Price) AS AveragePrice
	FROM tovar
		INNER JOIN postachalnik ON (tovar.id_postav = postachalnik.id_postach)
			GROUP BY postachalnik.Nazva
			