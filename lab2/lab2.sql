﻿CREATE DATABASE HospitalUniversity;
USE HospitalUniversity;


CREATE TABLE StudentCard(
id_cardStudent int NOT NULL primary key,
firstName nvarchar(50),
lastName nvarchar(50) ,
patramomic nvarchar(50),
faculty nvarchar(50),
groupNumber nvarchar(20),
residanceAddress nvarchar(50)
);

CREATE TABLE UniversityHospital(
id_hospital int NOT NULL primary key,
Town nvarchar(50),
Address nvarchar(50),
id_cardStudent int NOT NULL foreign key(id_cardStudent) references StudentCard(id_cardStudent) on delete cascade on update no action,
);

CREATE TABLE Doctor(
id_doctor int NOT NULL primary key,
firstName nvarchar(50),
lastName nvarchar(50),
patramomic nvarchar(50),
spacialty nvarchar(50),
roomNumber smallint,
workDate date
);

	CREATE TABLE Diagnos(
	id_diagnos int NOT NULL primary key,
	nameOfDisease nvarchar(50),
	description nvarchar(50),
	id_visit int NOT NULL ,
	id_doctor int NOT NULL foreign key (id_doctor) references Doctor(id_doctor) on delete cascade on update no action,
	);

CREATE TABLE DateVisit(
id_visit int NOT NULL primary key,
dateVisit datetime,
id_cardStudent int NOT NULL foreign key (id_cardStudent)  references StudentCard on delete cascade on update no action,
);

CREATE TABLE CardSickStudent(
id_cardSick int NOT NULL primary key,
id_doctor int ,
id_diagnos  int ,
id_visit int,
);

ALTER TABLE  DateVisit DROP CONSTRAINT [FK_DateVisit_StudentCard];

ALTER TABLE StudentCard DROP CONSTRAINT [PK__StudentC__40EC4AA59C9321E8];

ALTER TABLE DateVisit add primary key (id_cardStudent);

ALTER TABLE DateVisit add foreign key (id_visit) references StudentCard(id_cardStudent) on delete cascade on update no; 

ALTER TABLE Diagnos drop column id_visit;


SELECT * from Diagnos


INSERT INTO StudentCard(id_cardStudent, firstName, lastName, patramomic, faculty, groupNumber, residanceAddress)
Values(742, 'Дмитро', 'Дуб', 'Олексійович', 'ФІКТ', 'КБ-2', 'вул. Клосовского Олександра, 10-а' );
INSERT INTO StudentCard(id_cardStudent, firstName, lastName, patramomic, faculty, groupNumber, residanceAddress)
Values(743, 'Валентин', 'Куліш', 'Олександрович', 'ФІКТ', 'КБ-1', 'вул. Леха Качинского, 9' );
INSERT INTO StudentCard(id_cardStudent, firstName, lastName, patramomic, faculty, groupNumber, residanceAddress)
Values(744, 'Михайдо', 'Кондро', 'Володимирович', 'ФІКТ', 'ПІ-54', 'вул.Велика Бердичівська,б. 21' );
INSERT INTO StudentCard(id_cardStudent, firstName, lastName, patramomic, faculty, groupNumber, residanceAddress)
Values(745, 'Руслан ', 'Ганюк', 'Володимирович', 'ФІКТ', 'КБ-1', 'вул.Велика Бердичівська,б. 31а' );
INSERT INTO StudentCard(id_cardStudent, firstName, lastName, patramomic, faculty, groupNumber, residanceAddress)
Values(746, 'Олександр', 'Стельмащук', 'Олексійович', 'ФІКТ', 'КБ-2', 'вул.Велика Бердичівська,б. 32а' );
INSERT INTO StudentCard(id_cardStudent, firstName, lastName, patramomic, faculty, groupNumber, residanceAddress)
Values(747, 'Василь', 'Кресяк', 'Васильович', 'ФІКТ', 'КБ-3', 'вул. Клосовского Олександра, 4' );
INSERT INTO StudentCard(id_cardStudent, firstName, lastName, patramomic, faculty, groupNumber, residanceAddress)
Values(748, 'Любов', 'Шахрайчук', 'Євгенівна', 'ФІКТ', 'КБ-2', 'вул. Клосовского Олександра, 8-а' );
INSERT INTO StudentCard(id_cardStudent, firstName, lastName, patramomic, faculty, groupNumber, residanceAddress)
Values(749, 'Орест', 'Вітик', 'Дмитрович', 'ФІКТ', 'КБ-2', 'вул. Клосовского Олександра, 3-а' );


INSERT INTO UniversityHospital(id_hospital, Town, Address)
Values(12, 'Житомир', 'Романа Шухевича, 2А');
INSERT INTO UniversityHospital(id_hospital, Town, Address)
Values(23, 'Житомир', 'Червоного Хреста, 3');


INSERT INTO Doctor (id_doctor,firstName ,lastName , patramomic , spacialty, roomNumber, workDate)
VALUES (1, 'Марта ', 'Кравець ', 'Олегівна', 'кардіолог', 123, '2020-8-12' );
INSERT INTO Doctor (id_doctor,firstName ,lastName , patramomic , spacialty, roomNumber, workDate)
VALUES (2, 'Володимир  ', 'Коваль  ', 'Богданович', 'вірусолог', 109, '2020-2-11' );
INSERT INTO Doctor (id_doctor,firstName ,lastName , patramomic , spacialty, roomNumber, workDate)
VALUES (3, 'Ірина', 'Герега  ', 'Онуфріївна', 'педіатр', 108, '2020-3-2' );
INSERT INTO Doctor (id_doctor,firstName ,lastName , patramomic , spacialty, roomNumber, workDate)
VALUES (4, 'Анжеліка', 'Любченко', 'Олександрівна', 'вірусолог', 100 , '2020-4-4' );
INSERT INTO Doctor (id_doctor,firstName ,lastName , patramomic , spacialty, roomNumber, workDate)
VALUES (5, 'Іван', 'Левкін ', 'Володимирович', 'вірусолог', 102, '2020-8-12' );

INSERT INTO Diagnos (id_diagnos, nameOfDisease, description, id_doctor, id_visit)
VALUES (12, 'грип', 'температура 37,5, головна біль', 3, 8);
INSERT INTO Diagnos (id_diagnos, nameOfDisease, description, id_doctor, id_visit)
VALUES (13, 'грип', 'температура 37,5, головна біль', 3, 3);
INSERT INTO Diagnos (id_diagnos, nameOfDisease, description, id_doctor, id_visit)
VALUES (14, 'грип', 'температура 37,5, головна біль', 3, 2);
INSERT INTO Diagnos (id_diagnos, nameOfDisease, description, id_doctor, id_visit)
VALUES (15, 'грип', 'температура 37,5, головна біль', 3, 5);
INSERT INTO Diagnos (id_diagnos, nameOfDisease, description, id_doctor, id_visit)
VALUES (16, 'грип', 'температура 37,5, головна біль', 3, 4);
INSERT INTO Diagnos (id_diagnos, nameOfDisease, description, id_doctor, id_visit)
VALUES (17, 'грип', 'температура 37,5, головна біль', 3, 6);
INSERT INTO Diagnos (id_diagnos, nameOfDisease, description, id_doctor, id_visit)
VALUES (18, 'грип', 'температура 37,5, головна біль', 4, 9);
INSERT INTO Diagnos (id_diagnos, nameOfDisease, description, id_doctor, id_visit)
VALUES (19, 'грип', 'температура 37,5, головна біль', 2, 10);

INSERT INTO DateVisit(id_visit, dateVisit, id_cardStudent)
VALUES (3, '2020-03-04', 745);
INSERT INTO DateVisit(id_visit, dateVisit, id_cardStudent)
VALUES (2, '2020-03-11', 742);
INSERT INTO DateVisit(id_visit, dateVisit, id_cardStudent)
VALUES (5, '2020-03-13', 743);
INSERT INTO DateVisit(id_visit, dateVisit, id_cardStudent)
VALUES (8, '2020-03-14', 744);
INSERT INTO DateVisit(id_visit, dateVisit, id_cardStudent)
VALUES (4, '2020-03-18', 746);
INSERT INTO DateVisit(id_visit, dateVisit, id_cardStudent)
VALUES (6, '2020-03-20', 747);
INSERT INTO DateVisit(id_visit, dateVisit, id_cardStudent)
VALUES (9, '2020-03-21', 748);
INSERT INTO DateVisit(id_visit, dateVisit, id_cardStudent)
VALUES (10, '2020-03-23', 749);



