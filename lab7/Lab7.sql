use Teacher
create table Teacher( 
	id_teacher int IDENTITY(1,1) not null primary key,
	Name nvarchar(20) check (Name like '[�-�]%'),
	Surname nvarchar(20) check (Surname like '[�-�]%'),
	Phone char(12) check (Phone like '([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	Birth date,
	Family nvarchar(50) check (Family like '[�-�]%'),
	Disciplines nvarchar(50) check (Disciplines like '[�-�]%'),
	Street nvarchar(50),
	HomePhone char(12) check (HomePhone like '([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	Post nvarchar(50),
	id_grade int,
	id_workinghours int
);

create table faculty (
 id_faculty int  IDENTITY(1,1) not null primary key,
 FacultyName nvarchar(50) check (FacultyName like '[�-�]%'),
 Dean nvarchar(50) check (Dean like '[�-�]%'),
 DeanPhone char(12) check (DeanPhone like '([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
 AdministryPhone char(12) check (AdministryPhone like '([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
 FacultyId int,
 id_teacher int
);

create table Grade (
id_grade int  IDENTITY(1,1) not null primary key,
PHD nvarchar(50) check (PHD like '[�-�]%'),
PHDCandidate nvarchar(50) check (PHDCandidate like '[�-�]%'),
id_degree int
);

create table Degree (
id_degree int IDENTITY(1,1)  not null primary key,
Assistant  nvarchar(50) check (Assistant like '[�-�]%'),
Tutor  nvarchar(50) check (Tutor like '[�-�]%'),
Professor  nvarchar(50) check (Professor like '[�-�]%'),
Docent  nvarchar(50) check (Docent like '[�-�]%')
);

create table TypesOfClasses (
id_typesofclasses int IDENTITY(1,1) not null primary key,
Lecture  nvarchar(50) check (Lecture like '[�-�]%'),
Seminaries   nvarchar(50) check (Seminaries like '[�-�]%'),
Practice  nvarchar(50) check (Practice like '[�-�]%'),
Labs  nvarchar(50) check (Labs like '[�-�]%'),
Modules  nvarchar(50) check (Modules like '[�-�]%'),
Exams  nvarchar(50) check (Exams like '[�-�]%'),
Credit  nvarchar(50) check (Credit like '[�-�]%'),
Consults  nvarchar(50) check (Consults like '[�-�]%'),
Diploma  nvarchar(50) check (Diploma like '[�-�]%')
);

create table WorkingHours (
id_workinghours int IDENTITY(1,1) not null primary key,
Groups  nvarchar(50) check (Groups like '[�-�]%'),
Course int,
WorkHours int,
id_1 int
);

alter table Grade add foreign key (id_degree) references dbo.Degree (id_degree)on delete no action on update cascade
alter table Teacher add foreign key (id_grade) references dbo.Grade (id_grade) on delete no action on update cascade
alter table WorkingHours add foreign key (id_1) references dbo.TypesOfCLasses (id_typesofclasses) on delete no action on update cascade
alter table Teacher add foreign key (id_workinghours) references dbo.WorkingHours (id_workinghours)on delete no action on update cascade
alter table faculty add foreign key (id_teacher) references dbo.Teacher (id_teacher) on delete no action on update cascade



Insert into dbo.TypesOfClasses(Lecture,Seminaries,Practice,Labs,Modules,Exams,Credit,Consults,Diploma)
Values ('ϲ','��','ʲ','��','��','��','��','���','��'),('��','��','��','��','��','��','���','��','���'),('ϲ�','���','ʲ�','���','���','���','���','����','���'),
('ϲ�','���','ʲ�','���','���','���','���','��','���'),('ϲ�','���','ʲ�','���','���','���','���','��','���'),('ϲ�','���','ʲ�','���','���','���','���','����','���')


select * from TypesOfClasses

Insert into dbo.Degree(Assistant,Tutor,Professor,Docent)
Values ('ʲ �� ��','�� �� ��','������������ ����','���'),('��','��','��������� ����','��'),('ʲ','��','��������� ����','��'),
('��','���','��������','��'),('��','��','˳�������� ������','��'),('��','���','��','����')

select * from dbo.Degree

Insert into dbo.Grade(PHD,PHDCandidate)
Values ('� ��������','� ����������� �����'),('������������ ����','��� �������'),('Գ����� ��������','��������'),
('˳�������� ������','����ﳿ'),('��������� ���������','��� �������'),('�����㳿','������������'),('Գ�������','˳��������')

select * from dbo.Grade where PHDCandidate like '��� �������'

Insert into dbo.WorkingHours(Groups,Course,WorkHours,id_1)
Values ('��','2','1','1'),('ʲ','2','1','2'),('��','2','1','3'),('��','2','4','4'),('��','3','2','5'),('��','4','1','6'),('��','3','4','7')

select * from dbo.WorkingHours where WorkHours = '5' 

Insert into dbo.faculty(FacultyName,Dean,DeanPhone,AdministryPhone,FacultyId,id_teacher)
Values ('Բ��','����������� �.�.','(0673222281','(0683425283','125','19'),('�ʲ���','ϳ�������� �.�.','(0564327280','(0698495299','120','20'),
('����','������� �.�.','(0683242288','(0683525262','123','22'),('����','������� �.�.','(0503242351','(0633225284','126','21'),
('���','������� �.�.','(0703526280','(0493425382','128','25')

select * from dbo.faculty where FacultyId > '125' and FacultyName like '����'

Insert into dbo.Teacher(Name,Surname,Phone,Birth,Family,Disciplines,Street,HomePhone,Post,id_grade,id_workinghours)
Values ('������','��������','(1673222289','2000.10.10','�� ���������','��� ������','ϳ������','(1675325268','0','1','9'),
('�����','����������','(1693425425','1990.10.01','���������','���','ϳ������','(1505252468','��������','6','10'),
('���������','�����������','(1683225428','2000.05.01','���������','ʳ����������','������������','(2540525266','���.�������','3','8'),
('���������','������','(3643322869','1995.11.02','�� ���������','��','����������','(3605255268','�����','2','10'),
('��������','�����','(3633232648','1996.12.03','���������','��','���������','(3505525269','������� ��������','4','12'),
('�����','�������','(5653224728','1994.04.10','������','�����','�������','(4605556468','��������','5','10'),
('�����','������������','(7653826428','1997.04.12','�� ���������','Գ����� ��������','����������','(9625241681','������� ��������','6','10')

select * from dbo.Teacher

select * from dbo.TypesOfClasses

select Name,Surname,Street,Family,PHD,PHDCandidate from Teacher join Grade on Teacher.id_grade = Grade.id_grade

select Name,Family,WorkHours from dbo.Teacher join dbo.WorkingHours on 
Teacher.id_workinghours = WorkingHours.id_workinghours
where Family = '���������' or WorkHours = 4

select SUM(WorkingHours.Course) as sum_course from WorkingHours

select MAX(WorkHours) as maxHours from dbo.WorkingHours

select COUNT(Course) as CoursesNumber, Course from WorkingHours group by WorkingHours.Course

select AVG(WorkingHours.WorkHours) as avg_hours,WorkingHours.Groups from WorkingHours group by WorkHours,Groups

select * from dbo.Teacher where Street = (select Street from dbo.Teacher where Street = '���������')

select MIN(WorkHours) as minWork from dbo.WorkingHours

select count(WorkHours) as HourPerCourse FROM WorkingHours group by Course having Course > 2

SELECT Teacher.Disciplines, WorkingHours.Course From Teacher INNER JOIN WorkingHours ON Teacher.id_teacher = WorkingHours.id_workinghours

SELECT Teacher.Disciplines, WorkingHours.Course From Teacher INNER JOIN WorkingHours ON Teacher.id_teacher = WorkingHours.id_workinghours 
INNER JOIN faculty ON faculty.id_faculty = Teacher.id_teacher

declare @Speciality varchar(50) = '125'
SELECT FacultyName,	Dean FROM faculty WHERE FacultyId LIKE @Speciality;

select * from WorkingHours update WorkingHours set WorkHours = '0'

select * from faculty update faculty set FacultyName = 'Բ��', Dean = '����������� �.�.' where FacultyId = '120'

select *
from WorkingHours
declare @new int = 1 declare @old int = 0
declare @Course int = 4 update WorkingHours
set WorkingHours.WorkHours = (@new) where WorkingHours.WorkHours = (@old)
and	WorkingHours.Course = (@Course)

SELECT sysobjects.name AS �������, sysindexes.name AS ������, sysindexes.indid AS �����
FROM sysobjects INNER JOIN
 sysindexes ON sysobjects.id = sysindexes.id
WHERE (sysobjects.xtype = 'U') AND (sysindexes.indid > 0)
ORDER BY sysobjects.name, sysindexes.indid


create nonclustered index name on Teacher(Name)
with  fillfactor  = 50

select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'Teacher')


drop index name_surname on Teacher
create nonclustered index name_surname on Teacher(Name,Surname)


SELECT id_teacher, name FROM Teacher WHERE name = '��������' or id_grade = 1


create nonclustered index teacher_name on Teacher(name)
create nonclustered index whours on WorkingHours(WorkHours)
create nonclustered index faculty_name on faculty(FacultyName)
create nonclustered index dean_name on faculty(Dean)
create nonclustered index group_name on WorkingHours(Groups)


