﻿/* Куліш Михайло КБ-2-1 Варіант 8*/

CREATE DATABASE KR
Use KR;

CREATE TABLE Work(
id_Сipher int IDENTITY(1,1) not null primary key,
name nvarchar(30),
labourIntensity nvarchar(50),
closingDate date
);

CREATE TABLE  Employee (
id_personnelNumber int IDENTITY(1,1) not null primary key,
firstName nvarchar(30),
lastName nvarchar(30),
patronymic nvarchar(50),
position nvarchar(30)
);

CREATE TABLE Assignment(
id_number int IDENTITY(1,1) not null primary key,
dateOfIssue date,
labourIntensity nvarchar(50),
planDateEnd date,
realDateEnd date,
id_Сipher int,
id_personnelNumber int, 
FOREIGN KEY (id_Сipher)  REFERENCES  Work(id_Сipher),
FOREIGN KEY (id_personnelNumber)  REFERENCES Employee  (id_personnelNumber)
);



INSERT INTO Work( name, labourIntensity,closingDate)
VALUES('Адміністратор', '2','2000-01-09');
INSERT INTO Work( name, labourIntensity,closingDate)
VALUES('контроль систем обліку газу', '4','2000-05-12');
INSERT INTO Work( name, labourIntensity,closingDate)
VALUES('проектування механізованих', '5','2003-03-17');
INSERT INTO Work( name, labourIntensity,closingDate)
VALUES('проектування механізованих', '2','2004-03-17');
INSERT INTO Work( name, labourIntensity,closingDate)
VALUES('проектування механізованих', '3','2005-03-17');
INSERT INTO Work( name, labourIntensity,closingDate)
VALUES('проект Гелиограф', '1','2006-03-17');
INSERT INTO Work( name, labourIntensity,closingDate)
VALUES('проект Гелиограф', '1','2006-03-17');
INSERT INTO Work( name, labourIntensity,closingDate)
VALUES('проект Гелиограф', '1','2006-03-17');






INSERT INTO Employee(firstName, lastName, patronymic,position)
VALUES ('Дмитро', 'Дуб', 'Олексійович', 'інженер');
INSERT INTO Employee(firstName, lastName, patronymic,position)
VALUES ('Валентин', 'Куліш', 'Олександрович', 'інженер');
INSERT INTO Employee(firstName, lastName, patronymic,position)
VALUES ('Михайло', 'Кондро', 'Володимирович', 'інженер');
INSERT INTO Employee(firstName, lastName, patronymic,position)
VALUES ('Руслан ', 'Ганюк', 'Володимирович', 'інженер');
INSERT INTO Employee(firstName, lastName, patronymic,position)
VALUES ('Олександр', 'Дуб', 'Олексійович', 'адміністратор');
INSERT INTO Employee(firstName, lastName, patronymic,position)
VALUES ('Олександр', 'Палько', 'Олексійович', 'адміністратор');


INSERT INTO Assignment (dateOfIssue,labourIntensity, planDateEnd, realDateEnd)
VALUES ('1999-01-03', '3', '1999-01-04', '2000-01-03');
INSERT INTO Assignment (dateOfIssue,labourIntensity, planDateEnd, realDateEnd)
VALUES ('1999-01-04', '1', '1999-01-04', '2000-01-03');
INSERT INTO Assignment (dateOfIssue,labourIntensity, planDateEnd, realDateEnd)
VALUES ('1999-01-03', '1', '1999-01-04', '2000-01-03');

--Task 1
SELECT Work.name
FROM Work INNER JOIN Assignment ON Work.id_Сipher = Assignment.work
WHERE (((Work.labourIntensity) Between '1/1/2000' And '12/31/2000') AND ((Assignment.planDateEnd > Work <> closingDate))
GROUP BY Work.name;

--Task 3
SELECT Employee.position Count(Employee.id_personnelNumber) AS (count-personnelNumber)
FROM
Employee INNER JOIN 
Work.id_Сipher INNER JOIN Assignment ON Work.id = Assignment.work ON Employee.id_personnelNumber = Assignment.employee
WHERE Work ="Проект Гелиограф"
GROUP BY Employee.position;

--Task 4
SELECT Work.name, Sum(DateDiff('d',Assignment ! [dateOfIssue],[Work]![closingDate])) AS Day, Work.labourIntensity
FROM Work INNER JOIN Assignment ON Work.id_Cipher = Assignment.Work
GROUP BY Work.name, Work.labourIntensity
HAVING (((Sum(DateDiff('d',[Assignment]![dateOfIssue],[Work]![dateOfIssue])))<[Wokr]![labourIntensity]/2));








































